﻿param([string]$vaultName,
      [string]$secretName,
      [string]$outputCertPath)
try
{
    $response = (Invoke-WebRequest -Uri 'http://169.254.169.254/metadata/identity/oauth2/token?api-version=2018-02-01&resource=https%3A%2F%2Fvault.azure.net' -Method GET -Headers @{Metadata="true"}).Content | ConvertFrom-Json 
    $keyVaultToken = $response.access_token

    $secret = (Invoke-WebRequest -Uri https://$vaultName.vault.azure.net/secrets/$secretName/?api-version=7.0 -Method GET -Headers @{Authorization="Bearer $keyVaultToken"}).content | ConvertFrom-Json

    $kvSecretBytes = [System.Convert]::FromBase64String($secret.value)
    $certCollection = New-Object System.Security.Cryptography.X509Certificates.X509Certificate2Collection
    $certCollection.Import($kvSecretBytes,$null,[System.Security.Cryptography.X509Certificates.X509KeyStorageFlags]::Exportable)
 
    $pfxPath = $outputCertPath+"\client.pfx"
    [System.IO.File]::WriteAllBytes($pfxPath, $kvSecretBytes)
}
catch
{
    write-error $_.Exception
    exit 3
}